<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">SP+</h1>

            </div>
            <h3>Welcome to SP+</h3>
            <p>All you need to know about your perfomance
            </p>
            <p>Login in. To see it in action.</p>

           <?php if(!empty($error)) {?>

                <div class="alert alert-danger"><?= $error ?></div>

           <?php } ?>
            <form class="m-t" role="form" action="<?php echo base_url();?>index.php/Login/login" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" name="username" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href=""><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url();?>">Contact Manager</a>
            </form>
            <p class="m-t"> <small>Vortex solutions ltd &copy; 2014</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

</body>
</html>
