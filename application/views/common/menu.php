<body class="fixed-navigation">
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="<?php echo base_url();?>assets/img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $this->session->userdata('username');?></strong>
                             </span> <span class="text-muted text-xs block">Supplier<b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php echo base_url()?>">Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url()?>index.php/Welcome/logout">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        VA+
                    </div>
                </li>
                 <li>
                    <a href="<?php echo base_url();?>"><i class="fa fa-th-large"></i> <span class="nav-label">Purchase Orders</span>  </a>
                </li>
                   <li>
                    <a href="<?php echo base_url();?>index.php/Welcome/orders"><i class="fa fa-th-large"></i> <span class="nav-label">Statements/Invoices</span>  </a>
                </li>     
                <li>
                    <a href="metrics.html"><i class="fa fa-pie-chart"></i> <span class="nav-label">Sales</span>  </a>
                </li>
                <li>
                    <a href="widgets.html"><i class="fa fa-flask"></i> <span class="nav-label">Reports</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Graphs</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url();?>">Flot Charts</a></li>
                        <li><a href="<?php echo base_url();?>">Morris.js Charts</a></li>
                        <li><a href="<?php echo base_url();?>">Rickshaw Charts</a></li>
                        <li><a href="<?php echo base_url();?>">Chart.js</a></li>
                        <li><a href="<?php echo base_url();?>">Chartist</a></li>
                        <li><a href="<?php echo base_url();?>">c3 charts</a></li>
                        <li><a href="<?php echo base_url();?>">Peity Charts</a></li>
                        <li><a href="<?php echo base_url();?>">Sparkline Charts</a></li>
                    </ul>
                </li>             
              
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg sidebar-content">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
       
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to suppliers portal.</span>
                </li>
                  <li>
                    <a href="<?php echo base_url();?>index.php/Welcome/logout">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
               <!--  <li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li> -->
            </ul>

        </nav>
        </div>