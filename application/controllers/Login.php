<?php
class Login extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index($data = NULL)
		{
			$this->load->view('common/header',$data);
			$this->load->view('welcome',$data);
		}
	public function login()
		{

			$data['username'] = $this->security->xss_clean($this->input->post('username'));
			$data['password'] = $this->security->xss_clean($this->input->post('password'));

			$result = $this->Common_model->login($data);

			$response;

			if(!empty($result))
			{

				if($result->status == 0){

					$data["error"]="Inactive user";

					$this->index($data);

				}
				else{
					$session_data = array(
					'ID' => $result->ID,
					'username' => $result->username,
					'password' => $result->password,
					'supplierID'=>$result->supplierID,
					'status'=>$result->status,
					'isLoggedIn' =>TRUE,
					);

					$this->session->set_userdata($session_data);

					$data['main_content'] = 'Login successful';
					redirect('Welcome',$data);
				}

				

			}
			else
			{

				$data["error"]="Invalid username and Password combination";

				$this->index($data);

			}

		}
}

?>